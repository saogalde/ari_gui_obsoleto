// var plotoptions = {
// 	color 
// }

var popup;
var plotDaemon;
var freqDaemon;
var coordDaemon;
var refreshTime = 1000;
window.setInterval(function(){refreshHTMLClocks();},refreshTime);

function changeMode(mode){
	var isChanging = false;
	document.getElementById('welcome').style.display = "none";
	document.getElementById('stuff').style.display = "initial";
	switch(mode){
		case "single":
			document.getElementById('stuff').innerHTML = '<div id="spectra" class="plot"></div>';
			//initForNewMode('SD','SRT1');
			clearInterval(plotDaemon);
			clearInterval(freqDaemon);
			clearInterval(coordDaemon);
			plotDaemon = window.setInterval(function(){refreshHTMLPlots();},refreshTime);
			freqDaemon = window.setInterval(function(){refreshHTMLFreqs();},refreshTime);
			coordDaemon = window.setInterval(function(){refreshHTMLCoords();},refreshTime);
			changeStatusBlock('coordinates frequency time temperature');
			$('#singleimg').attr('src','res/icons/single_active.gif');
			$('#corrimg').attr('src','res/icons/corr.gif');
			$('#sumimg').attr('src','res/icons/sum.gif');
			break;
		case "intensity":
			document.getElementById('stuff').innerHTML = '<div id="spectra" class="plot"></div>';
			//initForNewMode('ARI','SH');
			clearInterval(plotDaemon);
			plotDaemon = window.setInterval(function(){refreshHTMLPlots();},refreshTime);
			freqDaemon = window.setInterval(function(){refreshHTMLFreqs();},refreshTime);
			coordDaemon = window.setInterval(function(){refreshHTMLCoords();},refreshTime);
			changeStatusBlock('coordinates frequency time temperature');
			$('#singleimg').attr('src','res/icons/single.gif');
			$('#corrimg').attr('src','res/icons/corr.gif');
			$('#sumimg').attr('src','res/icons/sum_active.gif');
			break;
		case "corr":
			document.getElementById('stuff').innerHTML = '<div id="spectra" class="plot"></div>';
			//initForNewMode('ARI','ROACH');
			clearInterval(plotDaemon);
			plotDaemon = window.setInterval(function(){refreshHTMLPlots();},refreshTime);
			freqDaemon = window.setInterval(function(){refreshHTMLFreqs();},refreshTime);
			coordDaemon = window.setInterval(function(){refreshHTMLCoords();},refreshTime);
			changeStatusBlock('coordinates frequency time temperature');
			$('#singleimg').attr('src','res/icons/single.gif');
			$('#corrimg').attr('src','res/icons/corr_active.gif');
			$('#sumimg').attr('src','res/icons/sum.gif');
			break;
	}
}

function refreshHTMLClocks(){
	document.getElementById('HTMLlst').innerHTML = currentClockObj.toLocaleTimeString();
	document.getElementById('HTMLutc').innerHTML = currentClockObj.getUTCHours() + ":"+ currentClockObj.getUTCMinutes()+ ":" + currentClockObj.getUTCSeconds();
}

function refreshHTMLFreqs(){
	document.getElementById('HTMLcenterFreq').innerHTML = freq.center;
	document.getElementById('HTMLspanFreq').innerHTML = freq.span;
}

function refreshHTMLCoords(){
	document.getElementById('HTMLaz').innerHTML = position.azel[0];
	document.getElementById('HTMLel').innerHTML = position.azel[1];
	document.getElementById('HTMLra').innerHTML = position.radec[0];
	document.getElementById('HTMLdec').innerHTML = position.radec[1];
}

function refreshHTMLPlots(){
	//$.plot("#instaspectra", [[[0, 12], [7, 12], null, [7, 2.5], [12, 2.5]]]);
	var finalData = data_srt1.readAndUpdate();
	$.plot("#spectra", 
		[
			{label: "spec", 	data : finalData[0]},
			{label: "intspec", 	data: finalData[1]}
		], 
		{
			yaxis: {
				axisLabel: 'Power Spectral Density (k·V^2·Hz^-1)',
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
				axisLabelPadding: 5,
				tickDecimals: 0,
				min: 0,
				transform: function(x){ return x/1000000;},
				tickFormatter: function(x){ return x/1000000;}
			},
			xaxis: {
				axisLabel: 'Frequency (MHz)',
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
    			axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
    			axisLabelPadding: 5,
    			min: freq.center - 0.5*freq.span,
    			max: freq.center + 0.5*freq.span,
			}/*,
			bars:{
				show: true,
				barWidth: freq.span*2/finalData[0].length,
				//align: center,
			}*/
		});
	console.log(freq.center - 0.5*freq.span, freq.center + 0.5*freq.span);
	
}

function changeStatusBlock(string){ // Available options: "coordinates", "frecuency", "time", "temperature", "velocities"
	command = string.split(" ");
	document.getElementById('statusset').innerHTML = '';
	for (var i = 0; i < command.length; i++) {
		if(command[i] == 'coordinates'){
			document.getElementById('statusset').innerHTML += '<div class="status">'+
				'<h3>Coordinates</h3>'+
				'<table class="statustable">'+
					'<tr><td class="tdicon"><img src="res/icons/azcoords.png"></td><td>azel</td>'+
					'<td id="HTMLaz"></td><td id="HTMLel"></td></tr>'+
					'<tr><td class="tdicon"><img src="res/icons/eqcoords.png"></td><td>radec</td>'+
					'<td id="HTMLra"></td><td id="HTMLdec"></td></tr>'+
					'<tr><td class="tdicon"><img src="res/icons/offsetcoords.png"></td><td>Offset</td>'+
					'<td id="HTMLoffaz"></td><td id="HTMLoffel"></td></tr></table></div>'
		}
		else if(command[i] == 'frequency'){
			document.getElementById('statusset').innerHTML += '<div class="status">'+
				'<h3>Frequencies</h3>'+
				'<table><tr><td>Center</td><td id="HTMLcenterFreq"></td><td>MHz</td></tr>'+
				'<tr><td>Span</td><td id="HTMLspanFreq"></td><td>MHz</td></tr></table></div>'
		}
		else if(command[i] == 'time'){
			document.getElementById('statusset').innerHTML += '<div class="status">'+
				'<h3>Time</h3>'+
				'<table><tr><td>UTC</td><td id="HTMLutc"></td></tr>'+
				'<tr><td>LST</td><td id="HTMLlst"></td></tr>'+
				'</table></div>'
		}
		else if(command[i] == 'temperature'){
			document.getElementById('statusset').innerHTML += '<div class="status">'+
				'<h3>Temperatures</h3>'+
				'<table><tr><td>tsys</td><td id="HTMLtsys"></td></tr>'+
				'<tr><td>trec</td><td id="HTMLtrec"></td></tr></table></div>'
		}
	};
}

function execCommand(string) {
	console.log("1");
	console.log(string);
	command = string.split(" ");
	c = command[0];
	if(c=='azel'){
		// execute command in ICE
		position.goToAzel(parseFloat(command[1]),parseFloat(command[2]));}
	else if(c=='radec'){
		position.goToRadec(parseFloat(command[1]),parseFloat(command[2]));
	}
	else if(c=='offset'){
		position.setOffset(parseFloat(command[1]),parseFloat(command[2]));
	}
	//else if(c=='target'){
		//API.SetTarget(command[1],1420.1,2);
		//API.StartTracking();}
	else if(c=='stow'){
		console.log("hola");
		position.goToAzel(0,0);
	}
	else if(c=='freq'){
		freq.changeFreq(command[1],parseFloat(command[2]),parseFloat(command[3]));
	}
}

function requestCoords(){
	popup = window.open("","MsgWindow","width=300,height=400");
	popup.document.write('<p>Enter coordinates:</p>');
	popup.document.write('<form name="coords">');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichcoord" id="azel">AZ EL</legend>');
	popup.document.write('<span>AZ</span><input type="number" name="az" value="0"><br>');
	popup.document.write('<span>EL</span><input type="number" name="el" value="0">');
	popup.document.write('</fieldset>');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichcoord" id="radec">RA DEC</legend>');
	popup.document.write('<span>RA</span><input type="number" name="ra" value="0"><br>');
	popup.document.write('<span>DEC</span><input type="number" name="dec" value="0">');
	popup.document.write('</fieldset>');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichcoord" id="tar">Or choose an -<strong><em>now visible</em></strong>- target:</legend>');
	popup.document.write('<select name="targetList"></select>')
	popup.document.write('</fieldset>');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="if(document.getElementById(\'azel\').checked){window.opener.execCommand(\'azel \'+String(document.coords.az.value)+\' \'+String(document.coords.el.value));}else if(document.getElementById(\'radec\').checked){window.opener.execCommand(\'radec \'+String(document.coords.ra.value)+\' \'+String(document.coords.dec.value));}else if(document.getElementById(\'tar\').checked){window.opener.execCommand(\'target \'+String(document.coords.targetList.value));}window.close();">');
	popup.document.write('<input type="button" name="cancel" Value="cancel" onClick="window.close();">');
	popup.document.write('</form>');
	targets = observableTargetsNow();
	console.log(popup);
	//
	//	console.log(popup.document.coordinates.elements())
	for (var i = targets.length - 1; i >= 0; i--) {
		l=popup.document.coords.targetList.options.length;
		var option = popup.document.createElement("option");
		option.text = targets[i];
		popup.document.coords.targetList.add(option);
	};
}

function requestFreq(){
	popup = window.open("","MsgWindow","width=300,height=400");
	popup.document.write('<p>Enter frequency:</p>');
	popup.document.write('<form name="freqs" id="currentForm">');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichfreq" id="censpan">Center Freq & Span</legend>');
	popup.document.write('<span>Center (MHz):</span><input type="number" name="center"><br>');
	popup.document.write('<span>Span (MHz):</span><input type="number" name="span" value="">');
	popup.document.write('</fieldset>');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichfreq" id="startstop">Start & Stop Frequencies</legend>');
	popup.document.write('<span>Start (MHz):</span><input type="number" name="start" value="0"><br>');
	popup.document.write('<span>Stop (MHz):</span><input type="number" name="stop" value="0">');
	popup.document.write('</fieldset>');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="if(document.getElementById(\'censpan\').checked){window.opener.execCommand(\'freq censpan \'+String(document.freqs.center.value)+\' \'+String(document.freqs.span.value));}else if(document.getElementById(\'startstop\').checked){window.opener.execCommand(\'freq startstop \'+String(document.freqs.start.value)+\' \'+String(document.freqs.stop.value));}window.close();">');
	popup.document.write('<input type="button" name="cancel" Value="cancel" onClick="window.close();">');
	popup.document.write('</form>');
	//document.getElementById("currentForm").elements["center"].value=window.opener.freq.center;
}


function popupCommand(command){
	console.log("popupCommand");
	switch(command){
		case 'coords':
			requestCoords();
			break;
		case 'offset':
			requestOffset();
			break;
		case 'freq':
			requestFreq();
			break;
		}
}

function openUniquePopup(command){
	console.log(command);
	//console.log(popup.closed, typeof(popup));
	if(typeof(popup)=='undefined' || popup.closed){
		popupCommand(command);
	}
	else{
		try{console.log("try");popup.document;}
		catch(e){popupCommand(command);}

		if (navigator.appName == 'Microsoft Internet Explorer') {
				popup.close();
				popupCommand(command);
			}
		else{popup.focus();}
	}
}


function requestOffset(){
	popup = window.open("","MsgWindow","width=300,height=200");
	popup.document.write('<p>Enter AZEL offsets:</p>');
	popup.document.write('<form name="azel">');
	popup.document.write('<fieldset>');
	popup.document.write('<span>AZ</span><input type="number" name="az" value="0"><br>');
	popup.document.write('<span>EL</span><input type="number" name="el" value="0">');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="execCommand("offset",this.azel.az.value,this.azel.el.value); window.close();">');
	popup.document.write('</fieldset>');
	popup.document.write('</form>');
}
