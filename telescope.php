<?php
include('session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>ARI Control</title>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="../../excanvas.min.js"></script><![endif]-->
	<!--<script src="jquery-1.9.1.min.js"></script>-->
	<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script src="js/model.js"></script>
	<script src="js/control.js"></script>
	<script src="js/cmdmenu.js"></script>
	<script src="js/jquery.tipsy.js"></script>
	<script language="javascript" type="text/javascript" src="flot/jquery.flot.js"></script>
	<script language="javascript" type="text/javascript" src="flot/jquery.flot.axislabels.js"></script>
	<!--<link rel="stylesheet" type="text/css" href="css/test-div.css">-->
	<link rel="stylesheet" type="text/css" href="css/final.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/tipsy.css">

</head>
<body>

	<div id="header">
		<div id="logohead">
			<h1>ARI Control</h1>
			<hr>
			<h2>Web Interface</h2>
		</div>
		<div id="userhead">
			<b id="logout"><a href="logout.php">Log Out</a></b>
		</div>
	</div>

	<nav>
		<ul class='cf'>
			<li><a href='#'>Observe</a>
				<ul>
					<li  onclick="openUniquePopup('coords');"><a href='#'>Go to coordinates...</a>
					</li>
					<li onclick="requestOffset();"><a href="#">Offset</a>
					</li>
				</ul>
			</li>
			<li><a href='#'>Calibration</a></li>
			<li><a href='#'>Settings</a>
				<ul>
					<li onclick="openUniquePopup('freq');"><a href='#'>Frequency...</a>
					</li>
				</ul>
			</li>
			<li onclick="if(confirm('Are you sure you want to go STOW?')){execCommand('stow');console.log('hola');}" class='last'><a href='#'>Stow</a></li>
		</ul>
	</nav>

	<div id="maincontainer">
		<div id="navmenu">
		<h2 style="text-align: center;">Modes</h2><hr>
				<a href="#"><img src="res/icons/single.gif" class="menuitem" id="singleimg" onclick="if(confirm('You are about to terminate your current mode, and enter to Single Dish Mode. Are you sure?')) changeMode('single')" title="Single Dish Mode"/></a><hr>
				<a href="#"><img src="res/icons/sum.gif" class="menuitem" id="sumimg" onclick="if(confirm('You are about to terminate your current mode, and enter to Adder Mode. Are you sure?')) changeMode('intensity')" title="Adder Mode"/></a><hr>
				<a href="#"><img src="res/icons/corr.gif" class="menuitem" id="corrimg"onclick="if(confirm('You are about to terminate your current mode, and enter to Correlator Mode. Are you sure?')) changeMode('corr')" title="Correlator Mode"/></a>
		<script type="text/javascript">
			$('.menuitem').tipsy({gravity: 'w', fade: true, delayOut: 100});
		</script>
		</div>

		<div id="welcome">
			<h1>Welcome to ARI SRT Control</h1>
			<h3>Choose the observation mode</h3>
		</div>



		<div id="stuff" style="display: none">
			<h1>Current Mode</h1>
			
			<div class="plotset">
				<div class="plotfull">
					<div class="plot"></div>
					<ul><li>Save</li></ul>
					<span>Save</span>
					<span>Save</span>
					<span>Save</span>
				</div>
				<div class="plotfull">
					<div class="plot"></div>
					<span>Save</span>
					<span>Save</span>
					<span>Save</span>
				</div>
			</div>
		</div>

		

		<div id="statusset">
			<div class="status">
				<h3>Coordinates</h3>
				<table class="statustable">
					<tr>
						<td class="tdicon"><img src="res/icons/azcoords.png"></td>
						<td>azel</td>
						<td id="HTMLaz"></td>
						<td id="HTMLel"></td>
					</tr>
					<tr>
						<td class="tdicon"><img src="res/icons/eqcoords.png"></td>
						<td>radec</td>
						<td id="HTMLra"></td>
						<td id="HTMLdec"></td>
					</tr>
					<tr>
						<td class="tdicon"><img src="res/icons/offsetcoords.png"></td>
						<td>Offset</td>
						<td id="HTMLoffaz"></td>
						<td id="HTMLoffel"></td>
					</tr>
				</table>
			</div>
			<div class="status">
				<h3>Frequencies</h3>
				<table>
					<tr>
						<td>Center</td>
						<td id="HTMLcenterFreq"></td>
						<td>MHz</td>
					</tr>
					<tr>
						<td>Span</td>
						<td id="HTMLspanFreq"></td>
						<td>MHz</td>
					</tr>
				</table>
			</div>
			<div class="status">
				<h3>Time</h3>
				<table>
					<tr>
						<td>UTC</td>
						<td id='HTMLutc'></td>
					</tr>
					<tr>
						<td>LST</td>
						<td id='HTMLlst'></td>
					</tr>
				</table>
			</div>
		</div>
		
		<div id="history">20.04.15 03:56:45: AZEL 45 50<br>
		20.04.15 03:56:45: AZEL 45 50<br>20.04.15 03:56:45: AZEL 45 50<br>20.04.15 03:56:45: AZEL 45 50<br></div>
	</div>


 	<div id="footer">
		ARI Control. Sebasthian Ogalde.
	</div>



</body>
</html>
